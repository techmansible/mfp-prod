Role Name
=========

Install IBM HTTP Server - Base + FP + Plugin

Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

Default:
ihs_path: "/software/WASSUPP"
ihs_fp_path: "/software/WASSUPPFP"
ihs_bin_path: "/software/WAS/WAS855"
ihs_fp_bin_path: "/software/WAS/WAS855FP"
ihs_install_xml:
 - { name: "IHS855.install.xml", log: "IHS855.install.log" }
 - { name: "PLG855.install.xml", log: "PLG855.install.log" }
ihs_installers:
 - { name: "WAS_V8.5.5_SUPPL_1_OF_3.zip", path: "/software/WAS/WAS855", dest: "{{ ihs_path }}" }
 - { name: "WAS_V8.5.5_SUPPL_2_OF_3.zip", path: "/software/WAS/WAS855", dest: "{{ ihs_path }}" }
 - { name: "WAS_V8.5.5_SUPPL_3_OF_3.zip", path: "/software/WAS/WAS855", dest: "{{ ihs_path }}" }
 - { name: "8.5.5-WS-WASSupplements-FP013-part1.zip", path: "/software/WAS/WAS855FP", dest: "{{ ihs_fp_path }}" }
 - { name: "8.5.5-WS-WASSupplements-FP013-part2.zip", path: "/software/WAS/WAS855FP", dest: "{{ ihs_fp_path }}" }
 - { name: "8.5.5-WS-WASSupplements-FP013-part3.zip", path: "/software/WAS/WAS855FP", dest: "{{ ihs_fp_path }}" }
bpm_home: "/opt/IBM/BPM"
ihs_home: "/was8/HTTPServer"
eclipse_tool_path: "/was8/IM/eclipse/tools"
web_log_path: "/weblogs/installogs"
ihs_version_file: "version.txt"


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: username.rolename, x: 42 }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
