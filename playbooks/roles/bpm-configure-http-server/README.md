Role Name
=========

This role performs the http-server configuration. 
Requirements
------------

Any pre-requisites that may not be covered by Ansible itself or the role should be mentioned here. For instance, if the role uses the EC2 module, it may be a good idea to mention in this section that the boto package is required.

Role Variables
--------------

Default:
bpm_home: "/opt/IBM/BPM"
bpm_misc_path: "/software/Misc"
was_admin_home: "/home/wasadmin"
was_admin_group: "wasgrp"
was_admin_user: "wasadmin"
pss_app_script_path: "/opt/PSS/SCRIPTS/APP"
httpd_conf: "httpd.conf"
ihs_home: "/was8/HTTPServer"
ihs_plugins: "/was8/Plugins"
http_admin_user: "htadmin"
http_admin_password: "Password1!"
admin_port: 443
http_keyfile: "{{ http_keystore_path }}/{{ sslCertAltServerName }}.kdb"
http_stashfile: "{{ http_keystore_path }}/{{ sslCertAltServerName }}.sth"
http_keystore_password: "Password1!"
http_keystore_path: "{{ ihs_home }}/conf/ssl/{{ sslCertAltServerName }}"
http_csrfile: "{{ http_keystore_path }}/{{ sslCertAltServerName }}.csr"
dn_parameters: 'CN={{ sslCertAltServerName }}.pru.intranet.asia,OU="Prudential Assurance Corporation Singapore",O="Prudential Corporation Asia",L="Central District",ST="Hong Kong",C=HK'

http_certs:
  - "root.crt"
  - "IntermediateCA.crt"
altServerName: "bpm-pacs-pc-dev"
altServerNameFQDN: "{{ altServerName }}.{{ ansible_domain }}"
sslCertAltServerName: "{{ altServerName }}"
sslCertPassword: "Password1!"
useSelfSignedCerts: False

was_plugin_driver: "{{ ihs_plugins }}/bin/64bits/mod_was_ap22_http.so"
web_server_py:
  - "mycreateunmanagedNode.py"
  - "mycreateWebServer.py"
  - "myModifyauditPolicy.py"
bpm_stop_scripts:
 - "{{ pss_app_script_path }}/bpmAppMaintenance.sh"
 - "{{ pss_app_script_path }}/bpmInfraMaintenance.sh"
bpm_start_scripts:
 - "{{ pss_app_script_path }}/bpmInfraMaintenance.sh"
 - "{{ pss_app_script_path }}/bpmAppMaintenance.sh"
admin_params:
 - { regex1: "@@SetupadmUser@@", replace: "{{ http_admin_user }}" }
 - { regex1: "@@SetupadmGroup@@", replace: "{{ was_admin_group }}" }
 - { regex1: "@@AdminPort@@", replace: "{{ admin_port }}" }


Dependencies
------------

A list of other roles hosted on Galaxy should go here, plus any details in regards to parameters that may need to be set for other roles, or variables that are used from other roles.

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: bpm-configure-http-server, become: yes }

License
-------

BSD

Author Information
------------------

An optional section for the role authors to include contact information, or a website (HTML is not allowed).
